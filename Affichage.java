import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

import javax.swing.*;


public class Affichage extends JPanel implements MouseMotionListener,MouseListener{
	
	private static final long serialVersionUID = 1L;
	public static final int ERREUR = 0;
	public static final int EPAISSEURCONTOUR = 10;
	public static final int NBBLOCLARG = 5;
	public static final int NBBLOCHAUT = 4;
	public static final int LARG = NBBLOCLARG*CoordBloc.XTAILLE+2*EPAISSEURCONTOUR+ERREUR;
	public static final int HAUT = NBBLOCHAUT*CoordBloc.YTAILLE+2*EPAISSEURCONTOUR+ERREUR;
	public static final int XMIN = EPAISSEURCONTOUR;
	public static final int YMIN = EPAISSEURCONTOUR;
	public static final int XMAX = LARG-EPAISSEURCONTOUR;
	public static final int YMAX = HAUT-EPAISSEURCONTOUR;
	public static final int DEPASSEMENT = 2*CoordBloc.XTAILLE;


	public Vector<Bloc> blocs;
	private int blocEnDeplacement;
	private int xDecalage;
	private int yDecalage;
	boolean gagne;
	private Fixation fixation;
	private Historique historique;
	int nbreEtapes;
	public JeuBlocs jeuBloc;
	private int xLecture;
	private int yLecture;
	private int nbLarge;
	private int nbHaut;
	boolean editeur;
	private boolean pasFini;
	String fichierNiveau;



	public Affichage(Vector<Bloc> blocs,JeuBlocs jeuBloc,boolean editeur) {
		this.jeuBloc = jeuBloc;
		this.pasFini = true;
		this.editeur = editeur;
		this.nbreEtapes = 0;
		this.setBackground(Color.CYAN);
		this.setPreferredSize(new Dimension(LARG+DEPASSEMENT,HAUT));
		this.fixation = new Fixation(blocs);
		this.blocs = blocs;
		this.blocEnDeplacement = -1;
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		this.gagne = false;
		this.historique = new Historique(this.blocs);
		if (!editeur)
			this.fichierNiveau = JeuBlocs.REPERTOIRE+"level"+jeuBloc.niveau+".levelbloc";
		this.repaint();
	}
	public void lireCoord(BufferedInputStream bis){
		int x,y,lecture;
		x=0;
		y=0;
		try {
			while ((lecture = bis.read()) != ','){
				x = x*10+lecture-'0';
			}

			while ((lecture = bis.read()) != JeuBlocs.LIGNESUIVANTE && lecture!=-1){
				y = y*10+lecture-'0';	
			}
		}catch (IOException e) {
			JOptionPane.showMessageDialog(this,"Le format du fichier est incorrect !", "erreur !",JOptionPane.ERROR_MESSAGE);
		}
		this.xLecture = (x*CoordBloc.XTAILLE+XMIN);
		this.yLecture = (y*CoordBloc.YTAILLE+YMIN);
	}

	public void lireNiveau(String fichier){
		File f = new File(fichier);
		try {
			FileInputStream fis = new FileInputStream(f);
			BufferedInputStream bis = new BufferedInputStream(fis);
			int lecture;
			while ((lecture = bis.read()) != -1){
				if (lecture == 'N'){
					bis.skip(6);
					this.lireCoord(bis);
					Bloc b = new Bloc(TypeBloc.NORMAL,xLecture,yLecture);
					this.blocs.add(b);
				}
				else if(lecture == 'H'){
					bis.skip(4);
					this.lireCoord(bis);
					Bloc b = new Bloc(TypeBloc.HAUT,xLecture,yLecture);
					this.blocs.add(b);
				}
				else if(lecture == 'L'){
					bis.skip(4);
					lecture = bis.read();
					if (lecture == 'E'){
						bis.skip(6);
						this.lireCoord(bis);
						Bloc b = new Bloc(TypeBloc.LARGEETHAUT,xLecture,yLecture);
						this.blocs.add(b);
					}
					else{
						this.lireCoord(bis);
						Bloc b = new Bloc(TypeBloc.LARGE,xLecture,yLecture);
						this.blocs.add(b);
					}
				}
			}
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(this, "le fichier "+fichier+" est introuvable !","erreur",JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this,"le fichier "+fichier+" est n'est pas accessible !","erreur",JOptionPane.ERROR_MESSAGE);
		}

	}

	public void mouseDragged(MouseEvent me) {
		if (gagne){
			this.gagne = false;
			int nbEtapesFinales = historique.terminerDeplacement();
			this.nbreEtapes+=nbEtapesFinales;
			this.jeuBloc.nbEtapes.setText("nombre d'�tapes = "+this.nbreEtapes);

			JOptionPane.showMessageDialog(this,"vous avez gagn� en "+this.nbreEtapes+" �tapes","bravo",JOptionPane.INFORMATION_MESSAGE);
			String fichier = fichierNiveau.substring(0, fichierNiveau.length()-FiltreFichierSauvegardeNiveau.EXTENSIONNIVEAU.length())+FiltreFichierSauvegardeHistorique.EXTENSIONHISTORIQUE;
			File f = new File(fichier);
			boolean resolutionMeilleur;
			resolutionMeilleur = false;
			if (f.exists()){
				FileInputStream fis;
				try {
					fis = new FileInputStream(f);
					BufferedInputStream bis = new BufferedInputStream(fis);
					Historique h = new Historique(blocs);
					h.litResolution(bis);
					if (this.nbreEtapes < h.evenements.size()){
						JOptionPane.showMessageDialog(this, "F�licitations, vous avez battu le record qui �tait de "+h.evenements.size()+" �tapes", "Record battu", JOptionPane.INFORMATION_MESSAGE);
						resolutionMeilleur = true;
					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else{				
				JOptionPane.showMessageDialog(this, "F�licitations, vous avez �tabli un premier record de "+this.historique.evenements.size()+" pour ce niveau ", "Nouveau Record !", JOptionPane.INFORMATION_MESSAGE);
				resolutionMeilleur = true;
			}
			if (resolutionMeilleur){
				this.historique.ecritHistorique(fichier);
			}
			niveauSuivant();
		}
		int x = me.getX();
		int y = me.getY();

		if (this.blocEnDeplacement == -1){
			int i;
			i = this.blocs.size()-1;
			while ((i >= 0) && (!this.blocs.elementAt(i).appartient(x, y))){
				i--;
			}
			if (i >= 0){
				if (this.blocs.elementAt(i).appartient(x, y)){
					this.blocEnDeplacement = i;
					this.historique.initialiserHistoriqueTemporaire(i);
				}
				this.xDecalage = x-blocs.elementAt(blocEnDeplacement).getX();
				this.yDecalage = y-blocs.elementAt(blocEnDeplacement).getY();			
				blocs.elementAt(blocEnDeplacement).setX(x-xDecalage);
				blocs.elementAt(blocEnDeplacement).setY(y-yDecalage);
			}

		}
		else{

			Bloc blocDepl = this.blocs.elementAt(blocEnDeplacement);

			x = x-xDecalage;
			y = y-yDecalage;

			Bloc tmp = new Bloc(blocDepl.getType(),x,blocDepl.getY());
			CoordBloc cb = new CoordBloc(tmp.getX(),tmp.getY(),tmp.getType());

			boolean testSUrSortie = (tmp.getType() == TypeBloc.LARGEETHAUT && (cb.getY2() <= 3*CoordBloc.YTAILLE+YMIN) && (cb.getY1() >= CoordBloc.YTAILLE+YMIN) && (cb.getX2() > XMAX-CoordBloc.XTAILLE/2)) && !editeur;

			if (((cb.getX1() > XMIN) && (cb.getX2() < XMAX) || testSUrSortie) && (Bloc.peutEtreDeplace(this.blocs,tmp,blocEnDeplacement) || this.editeur) && (this.blocs.elementAt(blocEnDeplacement).distanceCorrecteSurX(tmp,cb))){
				blocDepl.setX(x);
				this.repaint();
			}


			tmp = new Bloc(blocDepl.getType(),blocDepl.getX(),y);
			cb = new CoordBloc(tmp.getX(),tmp.getY(),tmp.getType());

			if (!((testSUrSortie) && (cb.getX2() > XMAX))){			
				if ((cb.getY1() > YMIN) && (cb.getY2() < YMAX) && (Bloc.peutEtreDeplace(this.blocs,tmp,blocEnDeplacement) || this.editeur) && (this.blocs.elementAt(blocEnDeplacement).distanceCorrecteSurY(tmp,cb))){
					blocDepl.setY(y);
					this.repaint();
				}
			}

			if (!this.editeur){
				int nbEtapesTmp = historique.gererHistorique();
				this.jeuBloc.nbEtapes.setText("nombre d'�tapes = "+(this.nbreEtapes+nbEtapesTmp));
			}
			//historique.afficheHistoriqueTmp();

		}
	}



	public void niveauSuivant() {
		reset();


		if (this.jeuBloc.fichierExterne){
			this.jeuBloc.fichierExterne = false;
		}
		else {
			this.jeuBloc.niveau++;
			if (this.jeuBloc.niveau > Application.LEVELMAX){
				this.repaint();
				JOptionPane.showMessageDialog(this,"Vous avez termin� le JEU","F�licitations",JOptionPane.INFORMATION_MESSAGE);
				JOptionPane.showMessageDialog(this,"Vous pouvez cr�er d'autres niveaux en choisissant 0 lors du choix du niveau au d�marrage du Jeu","Information",JOptionPane.INFORMATION_MESSAGE);			
				System.exit(0);
			}
		}
		this.fichierNiveau = JeuBlocs.REPERTOIRE+"level"+jeuBloc.niveau+".levelbloc";
		this.jeuBloc.genererNiveau(this.fichierNiveau);			
		this.repaint();
		jeuBloc.entreeNiveau.setText(jeuBloc.niveau+"");


	}
	public void reset() {
		this.blocs.removeAllElements();
		this.historique.evenements.removeAllElements();
		if (this.blocEnDeplacement != -1){
			this.blocEnDeplacement = -1;
		}
		this.jeuBloc.nbEtapes.setText("nombre d'�tapes = 0");
		this.nbreEtapes = 0;
		this.blocs.removeAllElements();	
	}

	public Historique getHistorique() {
		return historique;
	}
	public void paintComponent(Graphics g1) {	
		this.pasFini = true;
		super.paintComponent(g1);

		g1.setColor(Color.BLACK);
		g1.fillRect(0, 0,LARG,EPAISSEURCONTOUR);	
		g1.fillRect(0, YMAX,LARG,EPAISSEURCONTOUR);	
		g1.fillRect(0,0,EPAISSEURCONTOUR,HAUT);
		g1.fillRect(XMAX,0,EPAISSEURCONTOUR,HAUT);
		g1.setColor(Color.RED);
		g1.fillRect(XMAX,CoordBloc.YTAILLE+YMIN, EPAISSEURCONTOUR, 2*CoordBloc.YTAILLE);

		Graphics2D g = (Graphics2D) g1;

		Color c = Color.BLUE;
		Color d = Color.MAGENTA;

		for(int i = 0;i<this.blocs.size();i++){
			Bloc b = this.blocs.elementAt(i);
			if (b.getType() == TypeBloc.LARGEETHAUT){
				c = Color.GREEN;
				d = Color.BLUE;
			}
			CoordBloc cb = new CoordBloc(b.getX(),b.getY(),b.getType());
			if ((b.getType() == TypeBloc.LARGEETHAUT) && (cb.getX1()+CoordBloc.XTAILLE/4> XMAX))
				gagne = true;
			GradientPaint gp = new GradientPaint(cb.getX1(), cb.getY1(),c, cb.getX2(), cb.getY2(), d);//new Color((c.getRed()+511)/3,(c.getGreen()+511)/3,(c.getBlue()+511)/3), true);
			g.setPaint(gp);
			g.fillRect(cb.getX1(), cb.getY1(),cb.getX2()-cb.getX1(), cb.getY2()-cb.getY1());
			if (b.getType() == TypeBloc.LARGEETHAUT){
				c = Color.BLUE;
				d = Color.MAGENTA;
			}
		}
		this.pasFini = false;

	}



	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent arg0) {
		if (this.blocEnDeplacement != -1){
			int nbEtapesFinales = this.historique.terminerDeplacement();
			this.nbreEtapes+=nbEtapesFinales;
			if (!this.editeur)
				this.jeuBloc.nbEtapes.setText("nombre d'�tapes = "+this.nbreEtapes);

			this.fixation.fixer();
			//System.out.println(this.blocs.elementAt(blocEnDeplacement).toString());
			this.blocEnDeplacement = -1;
			this.repaint();
		}
	}
	public void genererEditeur() {
		this.blocs.removeAllElements();
		this.blocs.add(new Bloc(TypeBloc.LARGEETHAUT,XMIN,YMIN));
		for (int i = 0;i < this.nbLarge;i++){
			this.blocs.add(new Bloc(TypeBloc.LARGE,XMIN,YMIN+CoordBloc.YTAILLE));
		}
		for (int i = 0;i < this.nbHaut;i++){
			this.blocs.add(new Bloc(TypeBloc.HAUT,XMIN,YMIN+2*CoordBloc.YTAILLE));
		}
		int surface = 4+this.nbLarge*2+this.nbHaut*2;
		int nbNormal = NBBLOCHAUT*NBBLOCLARG-surface-2;
		for (int i = 0;i < nbNormal;i++){
			this.blocs.add(new Bloc(TypeBloc.NORMAL,XMIN+CoordBloc.XTAILLE,YMIN+2*CoordBloc.YTAILLE));
		}

	}
	public int getNbLarge() {
		return nbLarge;
	}
	public void setNbLarge(int nbLarge) {
		this.nbLarge = nbLarge;
	}
	public int getNbHaut() {
		return nbHaut;
	}
	public void setNbHaut(int nbHaut) {
		this.nbHaut = nbHaut;
	}
	public boolean pasFini() {
		return this.pasFini;
	}

}
