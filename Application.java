

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class Application {
	static final int LEVELMAX = 17;
	public static void main(String[] args){
		boolean tmp = false;
		int niveau = 1;		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		while (!tmp){
			String chaine = JOptionPane.showInputDialog(null,null,"Niveau (de 1 a "+LEVELMAX+" ,0 pour l'�diteur)",JOptionPane.QUESTION_MESSAGE);
			tmp = true;
			if (chaine == null){
				System.exit(0);
			}
			
			try{
				niveau = Integer.parseInt(chaine);
			}
			catch (Exception e){	
				tmp = false;
				JOptionPane.showMessageDialog(null ,"Saisie incorrecte","attention",JOptionPane.ERROR_MESSAGE);
			}
			if ((tmp) && ((niveau < 0) || (niveau > LEVELMAX)))
				tmp = false;
		}
		
		if (niveau == 0){
			new JeuBlocs(niveau,true);
		}
		else new JeuBlocs(niveau,false);
	}
}
