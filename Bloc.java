import java.util.Vector;


public class Bloc {
	TypeBloc type;

	public TypeBloc getType() {
		return type;
	}

	public void setType(TypeBloc type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "bloc("+x+","+y+")";
	}
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	int x,y;
	public Bloc(TypeBloc type,int x,int y) {
		this.type = type;
		this.x = x;
		this.y = y;
	}

	boolean appartient(int x,int y){

		CoordBloc c1;
		c1 = new CoordBloc(this.x,this.y,this.type);

		if ((x >= c1.getX1()) && (x <= c1.getX2())){
			if ((y >= c1.getY1()) && (y <= c1.getY2())){
				return true;
			}
			return false;
		}
		return false;
	}
	public static boolean peutEtreDeplace(Vector<Bloc> blocs,Bloc b,int indiceAIGnorer){


		int i;
		i = 0;
		boolean test ;
		test = true;
		CoordBloc c1 = new CoordBloc(b.getX(),b.getY(),b.getType());
		while ((i < blocs.size()) && (test)){
			Bloc tmp = blocs.elementAt(i);
			CoordBloc c2 = new CoordBloc(tmp.getX(),tmp.getY(),tmp.getType());
			test = (i == indiceAIGnorer) || (!b.appartient(c2.getX1(), c2.getY1())) && (!b.appartient(c2.getX2(), c2.getY1())) && (!b.appartient(c2.getX1(), c2.getY2())) && (!b.appartient(c2.getX2(), c2.getY2())) && (!tmp.appartient(c1.getX1(), c1.getY1())) && (!tmp.appartient(c1.getX2(), c1.getY1())) && (!tmp.appartient(c1.getX1(), c1.getY2())) && (!tmp.appartient(c1.getX2(), c1.getY2()));
			i++;
		}
		return test;
	}
	
	public boolean distanceCorrecteSurX(Bloc tmp,CoordBloc cbtmp){
		int x = Math.abs(cbtmp.getX2() - cbtmp.getX1());		
		int xcote = Math.abs(tmp.x - this.x);		
		return (xcote < x);
	}
	
	public boolean distanceCorrecteSurY(Bloc tmp,CoordBloc cbtmp){
		int y = Math.abs(cbtmp.getY2() - cbtmp.getY1());		
		int ycote = Math.abs(tmp.y - this.y);
		return (ycote < y);
	}
}
