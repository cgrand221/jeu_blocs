
public class CoordBloc {
	int x1,y1,x2,y2;//x1<x2
	public static final int XTAILLE = 100;
	public static final int YTAILLE = 50;
	public CoordBloc(int x,int y,TypeBloc type) {
		x1 = x;
		y1 = y;
		if (type == TypeBloc.NORMAL){
			x2 = x+XTAILLE-1;
			y2 = y+YTAILLE-1;
		}
		else if (type == TypeBloc.LARGE){
			x2 = x+2*XTAILLE-1;
			y2 = y+YTAILLE-1;
		}
		else if (type == TypeBloc.HAUT){
			x2 = x+XTAILLE-1;
			y2 = y+2*YTAILLE-1;
		}
		else{
			x2 = x+2*XTAILLE-1;
			y2 = y+2*YTAILLE-1;
		}
		
	}
	public int getX1() {
		return x1;
	}
	public int getY1() {
		return y1;
	}
	public int getX2() {
		return x2;
	}
	public int getY2() {
		return y2;
	}
}
