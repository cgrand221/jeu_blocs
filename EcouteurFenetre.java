import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JOptionPane;


public class EcouteurFenetre implements WindowListener {


	public void windowActivated(WindowEvent arg0) {
		;
	}


	public void windowClosed(WindowEvent arg0) {
		;
	}


	public void windowClosing(WindowEvent arg0) {
		int reponse;
		reponse = JOptionPane.showConfirmDialog(null, "etes vous sur de vouloir quitter ?","quitter",JOptionPane.YES_NO_OPTION);
		if (reponse == JOptionPane.YES_OPTION)
			System.exit(0);
	}


	public void windowDeactivated(WindowEvent arg0) {
		;
	}


	public void windowDeiconified(WindowEvent arg0) {
		;
	}


	public void windowIconified(WindowEvent arg0) {
		;
	}

	public void windowOpened(WindowEvent arg0) {
		;
	}

}
