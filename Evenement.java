
public class Evenement{
	int numBloc;
	private int x;
	private int y;
	
	public int getX() {
		return x;
	}
	
	public String toString() {
		
		return "bloc n�"+numBloc+"("+x+";"+y+")";
	}
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Evenement(int numBloc,int x,int y) {
		this.numBloc = numBloc;
		this.x = x;
		this.y = y;
	}
}
