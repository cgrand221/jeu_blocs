import java.io.File;

import javax.swing.filechooser.FileFilter;



public class FiltreFichierSauvegardeHistorique extends FileFilter{

	public static final String EXTENSIONHISTORIQUE = ".historybloc";

	
	public boolean accept(File f) {
		String s = f.getName();
		if (s.endsWith(EXTENSIONHISTORIQUE) || f.isDirectory()){
			return true;
		}
		return false;
	}

	
	public String getDescription() {		
		return "fichier Historique de blocs ("+EXTENSIONHISTORIQUE+")";
	}

}
