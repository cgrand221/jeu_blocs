import java.io.File;

import javax.swing.filechooser.FileFilter;


public class FiltreFichierSauvegardeNiveau extends FileFilter {
	public static final String EXTENSIONNIVEAU = ".levelbloc";
	
	public boolean accept(File f) {
		String s = f.getName();
		if (s.endsWith(EXTENSIONNIVEAU) || f.isDirectory()){
			return true;
		}
		return false;
	}

	
	public String getDescription() {
		return "Fichiers de sauvegarde de niveaux de Blocs("+EXTENSIONNIVEAU+")";
	}

}
