import java.util.Vector;


public class Fixation {

	private Vector<Bloc> blocs;

	public Fixation(Vector<Bloc> blocs) {
		this.blocs = blocs;
	}
	public void fixer(){
		int tmp;
		for (int i=0;i<blocs.size();i++){
			Bloc b = blocs.elementAt(i);
			int proximite = (b.getX() - Affichage.XMIN)%CoordBloc.XTAILLE;
			tmp =(b.getX() - Affichage.XMIN)/CoordBloc.XTAILLE;
			if (proximite > CoordBloc.XTAILLE/2){				
				b.setX((tmp+1)*CoordBloc.XTAILLE+Affichage.XMIN);
			}
			else b.setX(tmp*CoordBloc.XTAILLE+Affichage.XMIN);
			
			proximite = (b.getY() - Affichage.YMIN)%CoordBloc.YTAILLE;
			tmp =(b.getY() - Affichage.YMIN)/CoordBloc.YTAILLE;
			if (proximite > CoordBloc.YTAILLE/2){				
				b.setY((tmp+1)*CoordBloc.YTAILLE+Affichage.YMIN);
			}
			else b.setY(tmp*CoordBloc.YTAILLE+Affichage.YMIN);



		}
	}
}
