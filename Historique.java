import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JOptionPane;


public class Historique {

	Vector<Evenement> evenements;
	private Vector<Bloc> blocs;
	private Vector<Evenement> evenementstmp;
	private int blocEnDeplacement;
	private int xLecture;
	private int yLecture;
	public void lireCoord(BufferedInputStream bis){
		int x,y,lecture;
		x=0;
		y=0;
		try {
			while ((lecture = bis.read()) != ','){
				x = x*10+lecture-'0';
			}

			while ((lecture = bis.read()) != JeuBlocs.LIGNESUIVANTE && lecture!=-1){
				y = y*10+lecture-'0';	
			}
		}catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Le format du fichier est incorrect !", "erreur !",JOptionPane.ERROR_MESSAGE);
		}
		this.xLecture = (x*CoordBloc.XTAILLE+Affichage.XMIN);
		this.yLecture = (y*CoordBloc.YTAILLE+Affichage.YMIN);
		//System.err.println("("+x+","+y+")");
	}
	public void litResolution(BufferedInputStream bis){
		int lecture;
		try {
			lecture = bis.read();

			while ((lecture) != -1){
				int numBloc = 0;
				while ((lecture >= '0') && (lecture <= '9')){
					numBloc = numBloc*10 + lecture - '0';
					lecture = bis.read();
				}
				lireCoord(bis);
				this.evenements.add(new Evenement(numBloc,this.xLecture,this.yLecture));
				lecture = bis.read();
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"La r�solution pour ce niveau n'est pas disponible (acces refus�) !","R�solution indisponible",JOptionPane.ERROR_MESSAGE);
		}
	}

	public void ecritHistorique(String fichier){
		File f = new File(fichier);

		try {
			FileOutputStream fos = new FileOutputStream(f);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			int i = 0;
			while (i < this.evenements.size()){
				Evenement e = this.evenements.elementAt(i);
				String aEcrire = e.numBloc+" "+((e.getX()-Affichage.XMIN)/CoordBloc.XTAILLE)+","+((e.getY()-Affichage.YMIN)/CoordBloc.YTAILLE)+JeuBlocs.aLaLigne;
				bos.write(aEcrire.getBytes());
				i++;
			}
			bos.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,"Le fichier est introuvable !", "erreur !",JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Le fichier est prot�g� en �criture !", "erreur !",JOptionPane.ERROR_MESSAGE);
		}		
	}

	public Historique(Vector<Bloc> blocs) {
		this.blocs = blocs;
		this.evenements = new Vector<Evenement>();
	}

	public void ajouter(int x,int y){
		Evenement e = new Evenement(this.blocEnDeplacement,x,y);
		evenements.add(e);
	}

	public void initialiserHistoriqueTemporaire(int blocEnDeplacement){
		this.evenementstmp = new Vector<Evenement>();
		this.blocEnDeplacement = blocEnDeplacement;
		this.gererHistorique();
	}
	public int gererHistorique(){
		int x,y,tmp,i;

		Bloc b = blocs.elementAt(this.blocEnDeplacement);
		int proximite = (b.getX() - Affichage.XMIN)%CoordBloc.XTAILLE;
		tmp =(b.getX() - Affichage.XMIN)/CoordBloc.XTAILLE;
		if (proximite > CoordBloc.XTAILLE/2){				
			x = ((tmp+1)*CoordBloc.XTAILLE+Affichage.XMIN);
		}
		else x = (tmp*CoordBloc.XTAILLE+Affichage.XMIN);

		proximite = (b.getY() - Affichage.YMIN)%CoordBloc.YTAILLE;
		tmp =(b.getY() - Affichage.YMIN)/CoordBloc.YTAILLE;
		if (proximite > CoordBloc.YTAILLE/2){				
			y = ((tmp+1)*CoordBloc.YTAILLE+Affichage.YMIN);
		}
		else y = (tmp*CoordBloc.YTAILLE+Affichage.YMIN);




		i = 0;
		while ((i < evenementstmp.size()) && !(evenementstmp.elementAt(i).getX()==x && evenementstmp.elementAt(i).getY()==y)){
			i++;
		}
		if (i<evenementstmp.size()){
			for (int j=evenementstmp.size()-1;j>i;j--){
				evenementstmp.remove(j);
			}
			return evenementstmp.size()-1;
		}

		evenementstmp.add(new Evenement(blocEnDeplacement,x,y));
		return evenementstmp.size()-1;
	}

	public int terminerDeplacement() {
		//le premier evenement est le "rest� sur place du bloc"
		int nbDepl;
		for (int i = 1;i < this.evenementstmp.size();i++){
			this.evenements.add(this.evenementstmp.elementAt(i));
		}
		nbDepl = this.evenementstmp.size()-1;
		this.evenementstmp = null;
		return nbDepl;
	}
	public void afficheHistoriqueTmp(){
		for (int i=0;i < this.evenementstmp.size();i++){
			Evenement e = this.evenementstmp.elementAt(i);
			System.out.println("evenementtmp "+i+":"+e);
		}
	}
	public void afficheHistorique(){
		for (int i=0;i < this.evenements.size();i++){
			Evenement e = this.evenements.elementAt(i);
			System.out.println("evenement "+i+":"+e);
		}
	}
}
