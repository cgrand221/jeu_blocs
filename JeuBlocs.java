import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.swing.*;



public class JeuBlocs implements ActionListener, AdjustmentListener {
	int niveau;
	Vector<Bloc> blocs;
	public JLabel nbEtapes;
	JButton resolution;
	private Affichage affichage;
	private boolean editeur;
	private Resolvant resolvant;
	boolean resolutionEnCours;
	private JButton ouvrirNiveau;
	boolean fichierExterne;
	private JScrollBar vitesseResolution;
	int vitesse;
	JTextField entreeNiveau;
	private JButton apropos;
	public static final int LIGNESUIVANTE = 13;
	public static final Character aLaLigne = new Character(((char) LIGNESUIVANTE));
	public static final String REPERTOIRE = "./level/";



	public JeuBlocs(int niveau,boolean editeur) {
		this.fichierExterne = false;
		this.resolutionEnCours = false;

		JFrame f = new JFrame("Jeu des Blocs - par Christopher GRAND");
		this.blocs = new Vector<Bloc>();
		this.niveau = niveau;
		this.editeur = editeur;
		JPanel panneau = new JPanel();

		affichage = new Affichage(this.blocs,this,editeur);
		if (!editeur)
			genererNiveau(REPERTOIRE+"level"+niveau+".levelbloc");
		else {

			genererEditeur();
		}
		JPanel infos = new JPanel();

		nbEtapes = new JLabel("nombre d'�tapes = 0");
		if (!editeur){
			resolution = new JButton("r�soudre");
			infos.setLayout(new GridLayout(1,4));
		}
		else {
			resolution = new JButton("enregistrer");
			infos.setLayout(new GridLayout(1,3));
		}
		infos.add(nbEtapes);
		infos.add(resolution);

		this.ouvrirNiveau = new JButton("ouvrir un niveau");
		this.ouvrirNiveau.addActionListener(this);
		infos.add(ouvrirNiveau);

		resolution.addActionListener(this);
		this.vitesseResolution = new JScrollBar();
		this.vitesseResolution.setOrientation(JScrollBar.HORIZONTAL);
		this.vitesseResolution.setMinimum(1);
		this.vitesseResolution.setMaximum(110);
		this.vitesseResolution.setValue(50);
		this.vitesseResolution.setPreferredSize(new Dimension(200,5));
		
		JPanel vitesseRes = new JPanel();
		vitesseRes.setLayout(new GridLayout(1,2));
		vitesseRes.add(new JLabel("vitesse de r�solution :"));
		vitesseRes.add(this.vitesseResolution);
		this.vitesseResolution.addAdjustmentListener(this);
		
		infos.add(vitesseRes);
		JPanel infos2 = new JPanel();
		infos2.setLayout(new GridLayout(2,1));
		infos2.add(infos);
		infos2.add(vitesseRes);
		panneau.add(infos2);
		panneau.add(affichage);
		JPanel panneau3 = new JPanel();
		panneau3.setLayout(new GridLayout(1,2));

		panneau3.add(new JLabel("niveau (de 0 �  17) :"));
		JPanel panneau4 = new JPanel();
		panneau4.setLayout(new GridLayout(2,1));
		panneau4.add(panneau3);
		apropos = new JButton("A propos");
		apropos.addActionListener(this);
		panneau4.add(apropos);
		entreeNiveau = new JTextField(this.niveau+"");
		entreeNiveau.addActionListener(this);
		panneau3.add(entreeNiveau);
		panneau.add(panneau4);
		f.add(panneau);
		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		f.addWindowListener(new EcouteurFenetre());
		f.setSize(new Dimension(Affichage.LARG+Affichage.DEPASSEMENT+5,Affichage.HAUT+145));
		f.setVisible(true);
	}
	private void genererEditeur() {
		int nbHaut = 0,nbLarge = 0;
		boolean tmp;
		tmp = false;
		while (!tmp){
			String chaine = JOptionPane.showInputDialog(null,null,"nombre de blocs hauts (de 0 a 4)",JOptionPane.QUESTION_MESSAGE);
			tmp = true;
			if (chaine == null){
				System.exit(0);
			}

			try{
				nbHaut = Integer.parseInt(chaine);
			}
			catch (Exception e){	
				tmp = false;
				JOptionPane.showMessageDialog(null ,"Saisie incorrecte","attention",JOptionPane.ERROR_MESSAGE);
			}
			if ((tmp) && ((nbHaut < 0) || (nbHaut > 4)))
				tmp = false;
		}
		tmp = (nbHaut > 3);
		nbLarge = 0;
		while (!tmp){
			String chaine = JOptionPane.showInputDialog(null,null,"nombre de blocs larges (de 0 a "+(4-nbHaut)+")",JOptionPane.QUESTION_MESSAGE);
			tmp = true;
			if (chaine == null){
				System.exit(0);
			}

			try{
				nbLarge = Integer.parseInt(chaine);
			}
			catch (Exception e){	
				tmp = false;
				JOptionPane.showMessageDialog(null ,"Saisie incorrecte","attention",JOptionPane.ERROR_MESSAGE);
			}
			if ((tmp) && ((nbLarge < 0) || (nbLarge > 4-nbHaut)))
				tmp = false;
		}
		affichage.setNbHaut(nbHaut);
		affichage.setNbLarge(nbLarge);
		affichage.genererEditeur();

	}
	public void genererNiveau(String s){
		affichage.lireNiveau(s);
	}
	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == this.resolution){

			if (this.editeur){
				int i;
				JFileChooser jfc = new JFileChooser();
				jfc.setFileFilter(new FiltreFichierSauvegardeNiveau());
				jfc.setAcceptAllFileFilterUsed(false);
				if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
					String s = jfc.getSelectedFile().toString();
					if (!s.endsWith(FiltreFichierSauvegardeNiveau.EXTENSIONNIVEAU))
						s+=FiltreFichierSauvegardeNiveau.EXTENSIONNIVEAU;
					File fichier = new File(s);
					FileOutputStream fis=null;

					try {
						fis = new FileOutputStream(fichier);
						BufferedOutputStream bis= new BufferedOutputStream(fis);
						for (i=0;i<this.blocs.size();i++){
							Bloc b = blocs.elementAt(i);
							TypeBloc type = b.getType();
							int x = b.getX();
							int y = b.getY();
							String donnees = type+" "+((x-Affichage.XMIN)/CoordBloc.XTAILLE)+","+((y-Affichage.YMIN)/CoordBloc.YTAILLE)+aLaLigne;
							bis.write(donnees.getBytes());							
						}
						bis.close();					
					} catch (FileNotFoundException e) {
						JOptionPane.showMessageDialog(null, "fichier non trouv� !","fichier introuvable",JOptionPane.ERROR_MESSAGE);
					} catch (IOException e) {
						JOptionPane.showMessageDialog(null, "erreur : le fichier est inaccessible","erreur de droit d'acc�s",JOptionPane.ERROR_MESSAGE);
					}

				}
			}
			else{
				if (!this.resolutionEnCours){

					String fichier = affichage.fichierNiveau.substring(0, affichage.fichierNiveau.length()-FiltreFichierSauvegardeNiveau.EXTENSIONNIVEAU.length())+FiltreFichierSauvegardeHistorique.EXTENSIONHISTORIQUE;
					try {
						File f = new File(fichier);
						FileInputStream fis = new FileInputStream(f);
						BufferedInputStream bis = new BufferedInputStream(fis);
						affichage.reset();
						this.genererNiveau(affichage.fichierNiveau);
						Historique h = new Historique(blocs);
						h.litResolution(bis);
						resolvant = new Resolvant(affichage,h);
						resolvant.start();
						this.resolution.setText("stopper la r�solution");
						this.resolutionEnCours = true;
					} catch (FileNotFoundException e) {
						JOptionPane.showMessageDialog(null,"aucune r�solution n'est disponible pour ce niveau !","R�solution indisponible",JOptionPane.ERROR_MESSAGE);
					}
				}
				else{
					this.resolution.setText("r�soudre");
					this.resolutionEnCours = false;
					resolvant.stop();
					resolvant = null;
					affichage.reset();
					this.genererNiveau(affichage.fichierNiveau);
					this.affichage.repaint();
				}
			}
		}

		else if (ae.getSource() == this.ouvrirNiveau){
			if (!this.resolutionEnCours){
				JFileChooser jfc = new JFileChooser();
				jfc.setFileFilter(new FiltreFichierSauvegardeNiveau());
				jfc.setAcceptAllFileFilterUsed(false);
				if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
					String s = jfc.getSelectedFile().toString();
					if (!s.endsWith(FiltreFichierSauvegardeNiveau.EXTENSIONNIVEAU))
						s+=FiltreFichierSauvegardeNiveau.EXTENSIONNIVEAU;
					this.affichage.fichierNiveau = s;


					this.affichage.reset();	
					genererNiveau(this.affichage.fichierNiveau );				
					this.fichierExterne = true;
					//editeur = false;
					//affichage.editeur = false;
					//this.resolution.setText("r�soudre");
					this.entreeNiveau.setText("Niveau personnel");
					this.affichage.repaint();

				}
			}
			else JOptionPane.showMessageDialog(null,"Vous ne pouvez pas ouvrir un niveau pendant la r�solution automatique !" ,"information",JOptionPane.INFORMATION_MESSAGE);
		}
		
		if (ae.getSource() == this.entreeNiveau){
			if (!resolutionEnCours){
				try{
					this.niveau = Integer.parseInt(this.entreeNiveau.getText());
					if ((this.niveau > Application.LEVELMAX) || (this.niveau < 0)){
						JOptionPane.showMessageDialog(null,"Veuillez entrer un niveau entre 0 et "+Application.LEVELMAX+" !","Erreur de saisie",JOptionPane.ERROR_MESSAGE);
					}
					else if (this.niveau == 0){
						this.editeur = true;
						affichage.editeur = true;
						this.resolution.setText("enregistrer");
						
						genererEditeur();
						affichage.repaint();
					}
					else {
						editeur = false;
						affichage.editeur = false;
						this.resolution.setText("r�soudre");
						this.niveau--;
						this.fichierExterne = false;
						affichage.niveauSuivant();
					}						

				}
				catch (Exception e){
					if (!this.entreeNiveau.getText().equals(""))
						JOptionPane.showMessageDialog(null,"Veuillez entrer un niveau entre 0 et "+Application.LEVELMAX+" !", "Erreur de saisie",JOptionPane.ERROR_MESSAGE);
				}

			}
		}
		else if (ae.getSource() == apropos){
			JOptionPane.showMessageDialog(null,"A propos : cr�e par Christopher GRAND.", "A Propos",JOptionPane.INFORMATION_MESSAGE);
		}

	}
	
	public void adjustmentValueChanged(AdjustmentEvent ae) {
		if (ae.getSource() == this.vitesseResolution){
			try{
				Resolvant.temps = (-ae.getValue()*2000+200000)/99;
				
			}
			catch (Exception e) {
				;
			}
		}
	}
}
