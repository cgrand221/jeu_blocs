import javax.swing.JOptionPane;


public class Resolvant extends Thread {

	public static int temps = 1000;
	private Affichage affichage;
	private Historique historique;
	

	public Resolvant(Affichage affichage, Historique h) {
		this.affichage = affichage;
		this.historique = h;
	}

	public void run() {
		super.run();
		for (int i = 0;i < historique.evenements.size();i++){
			Evenement e = historique.evenements.elementAt(i);
			affichage.blocs.elementAt(e.numBloc).setX(e.getX());
			affichage.blocs.elementAt(e.numBloc).setY(e.getY());
			affichage.nbreEtapes++;
			affichage.jeuBloc.nbEtapes.setText("nombre d'�tapes = "+affichage.nbreEtapes);
			affichage.repaint();
			dormir();
		}
		JOptionPane.showMessageDialog(null,"vous avez gagn� en "+historique.evenements.size()+" �tapes","bravo",JOptionPane.INFORMATION_MESSAGE);
		affichage.jeuBloc.resolution.setText("r�soudre");
		affichage.jeuBloc.resolutionEnCours = false;
		affichage.gagne = false;
		affichage.niveauSuivant();
		
	}
	private void dormir() {
		while(affichage.pasFini());
		try {
			Thread.sleep(temps );
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}
}

